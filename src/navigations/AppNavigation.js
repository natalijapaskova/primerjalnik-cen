import { createAppContainer } from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer'
import {createStackNavigator} from 'react-navigation-stack'
/* import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import {createDrawerNavigator} from '@react-navigation/drawer' */
import HomeScreen from '../screens/Home/HomeScreen';
import CategoriesScreen from '../screens/Categories/CategoriesScreen';
import RecipeScreen from '../screens/Recipe/RecipeScreen';
import RecipesListScreen from '../screens/RecipesList/RecipesListScreen';
import DrawerContainer from '../screens/DrawerContainer/DrawerContainer';
import IngredientScreen from '../screens/Ingredient/IngredientScreen';
import SearchScreen from '../screens/Search/SearchScreen';
import IngredientsDetailsScreen from '../screens/IngredientsDetails/IngredientsDetailsScreen';
import Barcode from '../screens/Barcode/Barcode';
import EditTrgovina from '../screens/Edit/EditTrgovina';
import EditPassword from '../screens/Edit/EditPassword';
import EditArtikli from '../screens/Edit/EditArtikli';
import RegistrationScreen from '../screens/Registration/RegistrationScreen';
import LoginActivity from '../screens/Registration/LoginActivity';
import ProfileActivity from '../screens/Registration/ProfileActivity';
import ProdajalciList from '../screens/Prodajalci/ProdajalciList';
import ShowList from '../screens/Prodajalci/ShowList';
import Prodajalec from '../screens/Prodajalci/Prodajalec';
import SearchCh from '../screens/Search/searchCheapest';
import Maps from '../screens/Maps/Maps';
import AkcijaScreen from '../screens/primerjave/Akcija';
import ComparisonScreen from '../screens/primerjave/najvecArtiklov';



/* const Stack = createStackNavigator();

function MainNavigator() {
  return(
    <Stack.Navigator
      screenOptions={{
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
            alignSelf: 'center',
            flex: 1,
          }
      }}
    >
      <Stack.Screen name='Home' component={HomeScreen} />
      <Stack.Screen name='Categories' component={CategoriesScreen}/>
      <Stack.Screen name='Recipe' component={RecipeScreen}/>
      <Stack.Screen name='RecipesList' component={RecipesListScreen} />
      <Stack.Screen name='Ingredient' component={IngredientScreen} />
      <Stack.Screen name='Search' component={SearchScreen} />
      <Stack.Screen name='IngredientsDetails' component={IngredientsDetailsScreen} />
    </Stack.Navigator>
  )
} */

const MainNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Categories: CategoriesScreen,
    Recipe: RecipeScreen,
    RecipesList: RecipesListScreen,
    Ingredient: IngredientScreen,
    Search: SearchScreen,
    IngredientsDetails: IngredientsDetailsScreen,
    Barcode: Barcode,
    EditTrgovina: EditTrgovina,
    EditArtikli: EditArtikli,
    EditPassword: EditPassword,
    Registration: RegistrationScreen,
    LoginActivity: LoginActivity,
    ProfileActivity, ProfileActivity,
    ProdajalciList: ProdajalciList,
    ShowList: ShowList,
    Prodajalec: Prodajalec,

    ProfileActivity: ProfileActivity,
    Comparison: ComparisonScreen,
    Maps: Maps,
    Cheapest:SearchCh,
    Akcija: AkcijaScreen
    

  },
  {
    initialRouteName: 'Registration',
    // headerMode: 'float',
    defaulfNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1,
      }
    })
  }
); 

/* const Drawer = createDrawerNavigator();

function DrawerStack() {
  return(
    <Drawer.Navigator
      drawerPosition='left'
      initialRouteName='Main'
      drawerStyle={{
        width: 250
      }}
      drawerContent={props=> DrawerContainer}
    >
      <Drawer.Screen name='Main' component={MainNavigator} />
    </Drawer.Navigator>
  )
} */

// const DrawerStack = createDrawerNavigator(
//   {
//     Main: MainNavigator
//   },
//   {
//     drawerPosition: 'left',
//     initialRouteName: 'Main',
//     drawerWidth: 250,
//     contentComponent: DrawerContainer
//   }
// );
const DrawerStack = createDrawerNavigator(
  {
    Main: MainNavigator
  },
  {
    drawerPosition: 'left',
    initialRouteName: 'Main',
    drawerWidth: 250,
    contentComponent: DrawerContainer
  }
);


/* export default function AppContainer() {
  return(
    <NavigationContainer>
      <DrawerStack/>
    </NavigationContainer>
  )
} */
 
export default AppContainer = createAppContainer(DrawerStack);

console.disableYellowBox = true;