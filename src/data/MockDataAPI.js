
import { recipes, categories, ingredients } from './dataArrays';

import {trgovine} from '../screens/Home/HomeScreen';
import {artikli} from '../screens/Recipe/RecipeScreen';
import {artiklii} from '../screens/Barcode/Barcode';



export function getCategoryById(categoryId) {
  let category;
  trgovine.map(data => {
    if (data.id == categoryId) {
      category = data;
    }
  });
  return category;
}

export function getBarcode() {
  
  let array = art.map(data => {
   
  });
  return array;
  
  
}


export function getCategoryName(categoryId) {
  let name;
  trgovine.map(data => {
    if (data.id == categoryId) {
      name = data.naziv;
    }
  });
  return name;
}

export function getIngredientName(ingredientID) {
  let name;
  artiklii.map(data => {
    if (data.id == ingredientID) {
      name = data.naziv;
    }
  });
  return name;
}

export function getIngredientBarcode(ingredientID) {
  let name;
  artiklii.barcode.map(data => {
    if (data == ingredientID) {
      name = "yes";
    }
  });
  return name;
}

export function getIngredientUrl(ingredientID) {
  let url;
  artikli.map(data => {
    if (data.id == ingredientID) {
      url = data.slika;
    }
  });
  return url;
}


export function getRecipes(categoryId) {
  const recipesArray = [];
  trgovine.map(data => {
    if (data.categoryId == categoryId) {
      recipesArray.push(data);
    }
  });
  return recipesArray;
}

// modifica
export function getRecipesByIngredient(ingredientId) {
  const recipesArray = [];
  trgovine.map(data => {
    data.ingredients.map(index => {
      if (index[0] == ingredientId) {
        recipesArray.push(data);
      }
    });
  });
  return recipesArray;
}

export function getNumberOfRecipes(categoryId) {
  let count = 0;
  trgovine.map(data => {
    if (data.categoryId == categoryId) {
      count++;
    }
  });
  return count;
}

export function getAllIngredients(id) {
  const ingredientsArray = [];
    artikli.map(data => {
      if (data.trgovina_fk == id) {
        ingredientsArray.push(data);
      }
    });
  return ingredientsArray;
}

// functions for search
export function getRecipesByIngredientName(ingredientName) {
  const nameUpper = ingredientName.toUpperCase();
  const recipesArray = [];
  artikli.map(data => {
    if (data.name.toUpperCase().includes(nameUpper)) {
      // data.name.yoUpperCase() == nameUpper
      const recipes = getRecipesByIngredient(data.ingredientId);
      const unique = [...new Set(recipes)];
      unique.map(item => {
        recipesArray.push(item);
      });
    }
  });
  const uniqueArray = [...new Set(recipesArray)];
  return uniqueArray;
}

export function getRecipesByCategoryName(categoryName) {
  const nameUpper = categoryName.toUpperCase();
  const recipesArray = [];
  categories.map(data => {
    if (data.name.toUpperCase().includes(nameUpper)) {
      const recipes = getRecipes(data.id); // return a vector of recipes
      recipes.map(item => {
        recipesArray.push(item);
      });
    }
  });
  return recipesArray;
}

export function getRecipesByRecipeName(recipeName) {
  const nameUpper = recipeName.toUpperCase();
  const recipesArray = [];
  artikli.map(data => {
    if (data.naziv.toUpperCase().includes(nameUpper)) {
      recipesArray.push(data);
    }
  });
  return recipesArray;
}
