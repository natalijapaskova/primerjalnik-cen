import MenuImage from '../../components/MenuImage/MenuImage';
import DrawerActions from 'react-navigation';
import * as React from 'react';
import MapView, { Marker } from "react-native-maps";
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { Alert } from 'react-native';
import { color } from 'react-native-reanimated';

export let trgovine;

export default class Maps extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Maps',
    headerLeft: () => <MenuImage
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  });

  constructor(props) { 
    super(props);
  }


  render() {
    return (
    <View style={styles.container}>
        <MapView style={styles.map} 
      initialRegion={{
        latitude: 46.5547,
        longitude: 15.6459,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05
      }} 
      >
        <MapView.Marker
            coordinate={{latitude: 46.5574941,
            longitude: 15.6412016}}
            title={"Hofer"}
            description={"Vodnikov trg"}
            pinColor = {"blue"}

         />
          <MapView.Marker
            coordinate={{latitude: 46.5531671,
            longitude: 15.6299417}}
            title={"Hofer"}
            description={"Preradoviceva ulica 20"}
            pinColor = {"blue"}

         />
       <MapView.Marker
            coordinate={{latitude: 46.5610891,
            longitude: 15.6289295}}
            title={"Mercator"}
            description={"Koroska cesta 116"}
         />
           <MapView.Marker
            coordinate={{latitude: 46.5394395,
            longitude: 15.6406897}}
            title={"Mercator Center"}
            description={"Eve Lovse 1"}
         />
         <MapView.Marker
            coordinate={{latitude: 46.5602539,
            longitude: 15.6488030}}
            title={"Spar"}
            description={"Trg svobode 6"}
            pinColor = {"green"}
         />
         <MapView.Marker
            coordinate={{latitude: 46.5542067,
            longitude: 15.65333166  }}
            title={"Interspar"}
            description={"Pobreska cesta 18"}
            pinColor = {"green"}
         />
          <MapView.Marker
            coordinate={{latitude: 46.5519416,
            longitude: 15.6772968}}
            title={"Spar"}
            description={"Veljka Vlahovica 62"}
            pinColor = {"green"}
         />
          <MapView.Marker
            coordinate={{latitude: 46.5654819,
            longitude: 15.6222287}}
            title={"Lidl"}
            description={"Koroska cesta 173"}
            pinColor = {"yellow"}
         />
         <MapView.Marker
            coordinate={{latitude: 46.5267757,
            longitude: 15.6728812}}
            title={"Lidl"}
            description={"Ptujska cesta 192"}
            pinColor = {"yellow"}
         />
       </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    map: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
  });