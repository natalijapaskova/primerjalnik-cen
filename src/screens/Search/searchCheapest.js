// import React, {Component} from 'react';
// import { SearchBar } from 'react-native-elements';
import {trgovine} from '../Home/HomeScreen';
import {artikli} from '../Recipe/RecipeScreen';
// import {
//   FlatList,
//   ScrollView,
//   Text,
//   View,
//   Image,
//   TouchableHighlight
// } from 'react-native';
// export default class SearchCh extends React.Component {
//   state = {
//     search: '',
//   };

//   updateSearch = (search) => {
//     this.setState({ search });
//   };

//   render() {
//     const { search } = this.state;
//     var prices=[];
//     var fk=[];
//     var min = 0;
//     var index;
//     var name; 
//     var naziv =[];
//     artikli.map(item => {
//         if(item.naziv===search){
//           fk.push(item.trgovina_fk);
//           prices.push(item.cena);
//      } })
    
//     min = prices[0];
//     for(var i = 1; i<prices.length; i++){
//         if(prices[i]<min){
//             min = prices[i];
//             index = fk[i];
//         }
//     }

//   trgovine.map(item => {
//           naziv.push(item.naziv);
//            })
   
//    name = naziv[index-1];
     
//     return (
//       <View>
//       <SearchBar
//         placeholder="Type Here..."
//         onChangeText={this.updateSearch}
//         value={search}
//       />
      
//         <Text>{name}</Text>
//       </View>
//     );
//   }
// }

import React, {useState, useEffect} from 'react';

// import all the components we are going to use
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  FlatList
} from 'react-native';
import {SearchBar} from 'react-native-elements';

const SearchCh = () => {
  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [masterDataSource, setMasterDataSource] = useState([]);

  useEffect(() => {
    fetch('http://192.168.0.14/PrimerjalnikBaza/artikli.php')
      .then((response) => response.json())
      .then((responseJson) => {
        setFilteredDataSource(responseJson);
        setMasterDataSource(responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.naziv
          ? item.naziv.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({item}) => {
    return (
      // Flat List Item
      <Text
        style={styles.itemStyle}
        onPress={() => getItem(item)}>
          {item.id}
          {'.'}
          {item.naziv.toUpperCase()}
      </Text>
    );
  };

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: '#C8C8C8',
        }}
      />
    );
  };

  const getItem = (item) => {
    // Function for click on an item
     var prices=[];
    var fk=[];
    var min = 0;
    
    var name; 
    var naziv =[];
    artikli.map(i => {
        if(i.naziv===item.naziv){
          fk.push(i.trgovina_fk);
          prices.push(i.cena);
     } })
    
    min = prices[0];
    var index=fk[0];
    for(var i = 1; i<prices.length; i++){
        if(prices[i]<min){
            min = prices[i];
            index = fk[i];
        }
    }

  trgovine.map(i => {
          naziv.push(i.naziv);
           })
   
   name = naziv[index-1];
    alert('Izdelek ' + item.naziv + ' je najcenejši v ' +name + ' s ceno ' + min);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <SearchBar
          round
          searchIcon={{size: 24}}
          onChangeText={(text) => searchFilterFunction(text)}
          onClear={(text) => searchFilterFunction('')}
          placeholder="Piši tukaj..."
          value={search}
        />
        <FlatList
          data={filteredDataSource}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={ItemSeparatorView}
          renderItem={ItemView}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  itemStyle: {
    padding: 10,
  },
});

export default SearchCh;