import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MenuButton from '../../components/MenuButton/MenuButton';




export default class DrawerContainer extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.content}>
        <View style={styles.container}>
        {/* <MenuButton
            title="REGISTRACIJA"
            source={require('../../../assets/icons/register.png')}
            onPress={() => {
              navigation.navigate('Registration');
              navigation.closeDrawer();
            }}
          /> */}
          {/* <MenuButton
            title="TRGOVINE"
            source={require('../../../assets/icons/home.png')}
            onPress={() => {
              navigation.navigate('Home');
              navigation.closeDrawer();
            }}
          /> */}
          <MenuButton
            title="TRGOVINE"
            source={require('../../../assets/icons/home.png')}
            onPress={() => {
              navigation.navigate('Home');
              navigation.closeDrawer();
            }}
          />
          
          <MenuButton
            title="MAPA"
            source={require('../../../assets/icons/map.jpg')}
            onPress={() => {
              navigation.navigate('Maps');
              navigation.closeDrawer();
            }}
          />
          
          <MenuButton
            title="BARCODE SCANNER"
            source={require('../../../assets/icons/category.png')}
            onPress={() => {
              navigation.navigate('Barcode');
              navigation.closeDrawer();
            }}
          />
          <MenuButton
            title="ISKANJE NAJCENEJŠEGA"
            source={require('../../../assets/icons/search.png')}
            onPress={() => {
              navigation.navigate('Cheapest');
              navigation.closeDrawer();
            }}
          />
        {/*  <MenuButton
            title="Comparison"
            //source={require('../../../assets/icons/search.png')}
            onPress={() => {
              navigation.navigate('Comparison');
              navigation.closeDrawer();
               }}
              /> */}
           {/* <MenuButton
            title="PRODAJALCI"
            source={require('../../../assets/icons/prodajalci.jpg')}
            onPress={() => {
              navigation.navigate('ProdajalciList');
              navigation.closeDrawer();
            }}
          />  */}

        {/*  <MenuButton
            title="KOMPARACIJA"
            source={require('../../../assets/icons/comparison.jpg')}
            onPress={() => {
              navigation.navigate('Comparison');

            }}
            /> */}
          <MenuButton
            title="PREGLED UPORABNIKOV"
            source={require('../../../assets/icons/category.png')}
            onPress={() => {
              navigation.navigate('Categories');
              navigation.closeDrawer();
            }}
          />

          
          
        </View>
      </View>
    );
  }
}

DrawerContainer.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  })
};
