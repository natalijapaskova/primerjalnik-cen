import React from 'react';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Alert
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {
  FlatList,
  ScrollView,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
// import styles from './styles';
import {
  getIngredientUrl,
  getRecipesByIngredient,
  getCategoryName,
  getIngredientName
} from '../../data/MockDataAPI';
export let artiklii;
let name ;
 var naziv=[];
 var item;
export default class IngredientScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
     title: navigation.getParam('item'),
    }; 
  };

   constructor(props) {
    super(props);
    this.state = {
      items: []
     
    };
  }
  ratingCompleted(rating) {
    console.log("Rating is: " + rating)  
  }

 

    
   componentDidMount() {
    fetch('http://192.168.0.18/PrimerjalnikBaza/artikli.php')
      .then(res => res.json())
      .then(result => {
        this.setState({
          items: result
        });
      });
  }

  ratingInsert(rating) {
      
    fetch('http://192.168.0.18/PrimerjalnikBaza/insert-ocena.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({

      ocena : rating,

      artiklov_fk : 2


    })

    }).then((response) => response.json())
        .then((responseJson) => {

          // Showing response message coming from server after inserting records.
          Alert.alert(responseJson);

        }).catch((error) => {
          console.error(error);
        });

}

dodajvAkciji = (id) => {
      
    fetch('http://192.168.0.18/PrimerjalnikBaza/dodajvAkciji.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({

      id : id

    })

    }).then((response) => response.json())
        .then((responseJson) => {

          // Showing response message coming from server after inserting records.
          Alert.alert(responseJson);

        }).catch((error) => {
          console.error(error);
        });

}

brisiizAkciji = (id) => {
      
    fetch('http://192.168.0.14/PrimerjalnikBaza/brisiizAkciji.php', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({

      id : id

    })

    }).then((response) => response.json())
        .then((responseJson) => {

          // Showing response message coming from server after inserting records.
          Alert.alert(responseJson);

        }).catch((error) => {
          console.error(error);
        });

}

  onPressRecipe = item => {
    this.props.navigation.navigate('Recipe', { item });
  };

  renderRecipes = ({ item }) => (
        <View style={styles.container}>
          <Image style={styles.photoIngredient} source={{ uri: item.slika }} />
          <Text style={styles.title}>{item.naziv}</Text>
        </View>
  );
 

onPressIngredient = item => {
    var name = getIngredientName(item);
    let ingredient = item;
    const { navigation } = this.props;
    let title = 'Pregled produktov za ' + item.naziv;
    const id = navigation.getParam('item');
    this.props.navigation.navigate('Ingredient',{ item, id });
  };



  GetArtiklaIDFunction=(id,naziv, cena)=>{

    this.props.navigation.navigate('EditArtikli', { 

      id : id,
      naziv : naziv,
      cena : cena
      
    });

}
  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    
    const ingredientId = navigation.getParam('ingredient');
    const ingredientUrl = getIngredientUrl(ingredientId);
    const ingredientName = navigation.getParam('name');
     const i = navigation.getParam('item');
     const { items } = this.state;
     var naziv;
     var slika;
     var cena;
     var id;
    const name = '';
    items.map(item => {
          if(item.id===i){
            id=item.id
          naziv=item.naziv
          slika = item.slika
          cena = item.cena}})
             
  
  
         
    //const name = getIngredientName(item);
  // alert(`${name}`);

    return (
      <ScrollView style={styles.mainContainer}>
       
      
        
      
      <>
     
      
        <Card style={{backgroundColor: '#f1d1d1'}}>
          
            <Text style={styles.sectionTitle} style={{ color: '#eaafaf',fontWeight: 'bold' ,fontSize: 30,alignItems: 'center',justifyContent: 'center'}}> {naziv}  </Text>
           <Image style={styles.card} source={{ uri: slika }} />
        <Text style={styles.sectionTitle} style={{ color: '#eaafaf',fontWeight: 'bold' ,fontSize: 15,alignItems: 'center',justifyContent: 'center'}}> Cena: {cena}  </Text>
           
        </Card>
     

        <View>
        <Button
        title="Edit"
        onPress={() => {
          <Text>{naziv}</Text>
          navigation.navigate('EditArtikli',  {  
            id: id,  
            naziv: naziv,
            cena: cena,  })
          
       }}
      />
     <AirbnbRating
        count={5}
        reviews={["Bad", "Not Good", "Good", "Very Good", "Amazing"]}
        defaultRating={0}
        size={20}
        onFinishRating={this.ratingInsert}
    />
    <Button
            title='Dodaj v akciji'
            color={this.state.myColor}
            size= {20}
            style={{margin:40,}}
           onPress={() => this.dodajvAkciji(id)}                                
            />

             <Button
            title='Brisi iz akciji'
            color ="red"
            size= {20}
            style={{margin:40}}
           onPress={() => this.brisiizAkciji(id)}                                
            />
         </View>
    </>

     </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 16,
    alignItems: 'center', // Centered horizontally
    color:'#f1d1d1',
    backgroundColor: '#f1d1d1',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
    justifyContent: 'center', //Centered vertically
    alignItems: 'center', 
  },
  card: {
    height: 200,
    width: '100%',
    backgroundColor: '#f1d1d1',
      color:'#f1d1d1',
    justifyContent: 'center', //Centered vertically
    alignItems: 'center', // Centered horizontally
  },
});
