import React, { Component } from 'react';
import styles from './styles';
import { StyleSheet, View, Alert, TextInput, Button, Text, Platform, TouchableOpacity, ListView, ActivityIndicator } from 'react-native';
import { StackNavigator } from 'react-navigation';


class ProdajalciList extends Component {

    static navigationOptions =
    {
       title: 'Prodajalci',
    };
  
  constructor(props) {
  
     super(props)
  
     this.state = {
  
        ime: '',
        priimek: '',
      
  
     }
  
   }
  
   InsertStudentRecordsToServer = () =>{
  
        fetch('http://192.168.0.18/PrimerjalnikBaza/insert-prodajalci.php', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
  
          ime : this.state.ime,
  
          priimek : this.state.priimek
  
  
        })
  
        }).then((response) => response.json())
            .then((responseJson) => {
  
              // Showing response message coming from server after inserting records.
              Alert.alert(responseJson);
  
            }).catch((error) => {
              console.error(error);
            });
  
  }
  
   GoTo_Show_StudentList_Activity_Function = () =>
    {
      this.props.navigation.navigate('ShowList');
      
    }
  
   render() {
     return (
  
  <View style={styles.MainContainer}>
  
  
         <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Dodaj prodajalci </Text>
   
         <TextInput
           
           placeholder="Vpišite ime prodajalec"
  
           onChangeText={ TextInputValue => this.setState({ ime : TextInputValue }) }
  
           underlineColorAndroid='transparent'
  
           style={styles.TextInputStyleClass}
         />
  
        <TextInput
           
           placeholder="Vpišite priimek prodajalec"
  
           onChangeText={ TextInputValue => this.setState({ priimek : TextInputValue }) }
  
           underlineColorAndroid='transparent'
  
           style={styles.TextInputStyleClass}
         />
  
        
  
        <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.InsertStudentRecordsToServer} >
  
          <Text style={styles.TextStyle}> DODAJ</Text>
  
        </TouchableOpacity>
  
        <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.GoTo_Show_StudentList_Activity_Function} >
  
          <Text style={styles.TextStyle}> PREGLED VSE PRODAJALCI </Text>
  
        </TouchableOpacity>
   
  
  </View>
             
     );
   }
  }

  export default ProdajalciList;