import React, { Component } from 'react';
import styles from './styles';
import ListView from "deprecated-react-native-listview";
import { StyleSheet, View, Alert, TextInput, Button, Text,Platform, TouchableOpacity, ActivityIndicator, ScrollView, FlatList, TouchableHighlight} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Card, ListItem,  Icon } from 'react-native-elements'



export let prodajalci;



export default class ShowList extends Component {

  static navigationOptions =
  {
     title: 'Prikaz vse prodajaci',
  };

  constructor(props) {

      super(props);

      this.state = {

        isLoading: true
      }
    }
   
   
    webCall=()=>{ 
      return fetch('http://192.168.0.18/PrimerjalnikBaza/show-prodajalciList.php')
      .then((response) => response.json())
         .then((responseJson) => {
           this.setState({
             isLoading: false,
             dataSource: responseJson
           }, function() {
            return prodajalci=responseJson;
             // In this block you can do something with new state.
           });
         })
         .catch((error) => {
           console.error(error);
         });}
    



    componentDidMount(){
 
      this.webCall();
    }
   
    renderRecipes = ({ item }) => (
    
      <TouchableHighlight underlayColor='rgba(73,182,77,0.9)' onPress={() => this.onPressRecipe(item)}>
        <View style={styles.container}>
          
          <Text style={styles.title}>{item.ime}</Text>
          <Text style={styles.category}>{item.priimek}</Text>
  
        </View>
      </TouchableHighlight>
    );
  
    render() {
      return (
        <View>
          <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            numColumns={2}
            data={this.state.dataSource}
            renderItem={this.renderRecipes}
            keyExtractor={item => `${item.id}`}
          />
        </View>
      );
    }
  }
  