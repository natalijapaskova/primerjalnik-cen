import React from 'react';

import { Card, ListItem, Button, Icon } from 'react-native-elements'
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {
  FlatList,
  ScrollView,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
 import styles from './styles';
import {
  getIngredientUrl,
  getRecipesByIngredient,
  getCategoryName,
  getIngredientName
} from '../../data/MockDataAPI';


let name ;
 var naziv=[];
 var item;
export default class Prodajalec extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
     title: navigation.getParam('item'),
    }; 
  };

   constructor(props) {
    super(props);
    this.state = {
      items: []
     
    };
  }

    
   componentDidMount() {
    fetch('http://192.168.0.107/PrimerjalnikBaza/show-prodajalci.php')
      .then(res => res.json())
      .then(result => {
        this.setState({
         
         
          items: result
        });
      });
  }

  onPressRecipe = item => {
    this.props.navigation.navigate('Prodajalec', { item });
  };

  renderRecipes = ({ item }) => (
        <View style={styles.container}>
          <Image style={styles.photoIngredient} source={{ uri: item.slika }} />
          <Text style={styles.title}>{item.ime}</Text>
        </View>
  );
 

onPressIngredient = item => {
    var name = getIngredientName(item);
    let ingredient = item;
    const { navigation } = this.props;
    let title = 'Pregled produktov za ' + item.naziv;
    const id = navigation.getParam('item');
    this.props.navigation.navigate('Prodajalec',{ item, id });
  };


  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    
    const ingredientId = navigation.getParam('ingredient');
    const ingredientUrl = getIngredientUrl(ingredientId);
    const ingredientName = navigation.getParam('name');
     const i = navigation.getParam('item');
     const { items } = this.state;
     var ime;
    ;
     var priimek;
     var id;
    const name = '';
    items.map(item => {
          if(item.id===i){
            id=item.id
          ime=item.ime
          priimek = item.priimek
          }})
             
  
  
         
    //const name = getIngredientName(item);
  // alert(`${name}`);

    return (
      <ScrollView style={styles.mainContainer}>
       
      
        
      
      <>
     
      
        <Card style={{backgroundColor: '#f1d1d1'}}>
          
            <Text style={styles.sectionTitle} style={{ color: '#eaafaf',fontWeight: 'bold' ,fontSize: 30,alignItems: 'center',justifyContent: 'center'}}> {ime}  </Text>
           
        <Text style={styles.sectionTitle} style={{ color: '#eaafaf',fontWeight: 'bold' ,fontSize: 15,alignItems: 'center',justifyContent: 'center'}}>  {priimek}  </Text>
           
        </Card>
     
    </>

     </ScrollView>
    );
  }
}

