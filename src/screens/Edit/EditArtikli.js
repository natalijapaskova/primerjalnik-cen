
import React, { Component } from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {
  FlatList,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  Platform,
  ListView,
  ActivityIndicator ,
  Alert,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import {
  getIngredientName,
  getAllIngredients,
} from '../../data/MockDataAPI';
import { TextInput } from 'react-native-gesture-handler';




export default  class EditArtikli extends Component {
  
    constructor(props) {
      
         super(props)
      
         this.state = {
      
           id: '',
           naziv: '',
           cena: '',
           
           
      
         }
      
       }
   
       componentDidMount(){
   
        this.setState({ 
          id : this.props.navigation.state.params.id,
          naziv: this.props.navigation.state.params.naziv,
          cena: this.props.navigation.state.params.cena,
       
          
        })
   
       }
    
      static navigationOptions =
      {
         title: 'EditArtikli',
      };
   
      UpdateRecord = () =>{
        
              fetch('http://192.168.0.107/PrimerjalnikBaza/update-artikli.php', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
        
                id : this.state.id,
   
                naziv : this.state.naziv,
        
                cena : this.state.cena
        
        
              })
        
              }).then((response) => response.json())
                  .then((responseJson) => {
        
                    // Showing response message coming from server updating records.
                    Alert.alert(responseJson);
        
                  }).catch((error) => {
                    console.error(error);
                  });

                  this.props.navigation.navigate('Ingredient');
        
        }


        
     



  
      render() {
  
        return (
     
     <View style={styles.MainContainer}>
     
            <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Urejaj podatke </Text>
      
            <TextInput
              
              placeholder="Naziv artikla"
              
              value={this.state.naziv}
     
              onChangeText={ TextInputValue => this.setState({ naziv : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
           <TextInput
              
              placeholder="Cena artikla"
  
              value={this.state.cena}
     
              onChangeText={ TextInputValue => this.setState({ cena : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />


           
     
            
           <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UpdateRecord} >
     
              <Text style={styles.TextStyle}> UPDATE  </Text>
     
           </TouchableOpacity>

     
     
     </View>
                
        );
      }
  
  }
  

