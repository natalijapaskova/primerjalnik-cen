
import React, { Component } from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {
  FlatList,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  Platform,
  ListView,
  ActivityIndicator ,
  Alert,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import {
  getIngredientName,
  getAllIngredients,
} from '../../data/MockDataAPI';
import { TextInput } from 'react-native-gesture-handler';




export default  class EditTrgovina extends Component {
  
    constructor(props) {
      
         super(props)
      
         this.state = {
      
           id: '',
           naziv: '',
           lokacija: '',
           koordinati: '',
           
      
         }
      
       }
   
       componentDidMount(){
   
        // Received Student Details Sent From Previous Activity and Set Into State.
        this.setState({ 
          id : this.props.navigation.state.params.id,
          naziv: this.props.navigation.state.params.naziv,
          lokacija: this.props.navigation.state.params.lokacija,
          koordinati: this.props.navigation.state.params.koordinati,
          
        })
   
       }
    
      static navigationOptions =
      {
         title: 'EditTrgovina',
      };
   
      UpdateRecord = () =>{
        
              fetch('http://192.168.0.107/PrimerjalnikBaza/update-trgovina.php', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
        
                id : this.state.id,
   
                naziv : this.state.naziv,
        
                lokacija : this.state.lokacija,
        
               koordinati : this.state.koordinati
        
               
        
              })
        
              }).then((response) => response.json())
                  .then((responseJson) => {
        
                    // Showing response message coming from server updating records.
                    Alert.alert(responseJson);
        
                  }).catch((error) => {
                    console.error(error);
                  });

                  this.props.navigation.navigate('Recipe');
        
        }



  
      render() {
  
        return (
     
     <View style={styles.MainContainer}>
     
            <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Urejaj podatke </Text>
      
            <TextInput
              
              placeholder="Naziv trgovina"
              
              value={this.state.naziv}
     
              onChangeText={ TextInputValue => this.setState({ naziv : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
           <TextInput
              
              placeholder="Lokacija trgovina"
  
              value={this.state.lokacija}
     
              onChangeText={ TextInputValue => this.setState({ lokacija : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
           <TextInput
              
              placeholder="Koordinati trgovina"
  
              value={this.state.koordinati}
     
              onChangeText={ TextInputValue => this.setState({ koordinati : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
            
           <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UpdateRecord} >
     
              <Text style={styles.TextStyle}> UPDATE  </Text>
     
           </TouchableOpacity>
     
           
      
     
     </View>
                
        );
      }
  
  }
  

