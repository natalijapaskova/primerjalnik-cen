import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';




const styles = StyleSheet.create({
  container: RecipeCard.container,
  photo: RecipeCard.photo,
  title: RecipeCard.title,
  category: RecipeCard.category,
  root:{
    flex:1,
 },
 inputStyle:{
     margin:5
 },
 modalView:{
     position:"absolute",
     bottom:2,
     width:"100%",
     backgroundColor:"white"

 },
 modalButtonView:{
     flexDirection:"row",
     justifyContent:"space-around",
     padding:10
 } ,

 contentContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  segmentButton: {
    flex: 1,
    justifyContent: 'center'
  },
  form: {
    flex: 9,
    marginBottom: 20
  },
  closeIconItem: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  closeIcon: {
    color: 'red'
  },
  rowSpan1: {
    flex: 1,
    marginTop: 10
  },
  rowSpan3: {
    flex: 4,
    marginTop: 10
  },
  button: {
    marginTop: 10
  },
  container: {
      flex: 2
  },
  item: {
      borderBottomWidth: 1,
      borderBottomColor: 'grey',
      alignItems: 'flex-start'
  },
  text:{
  marginVertical: 30,
  fontSize: 25,
  fontWeight: 'bold',
  marginLeft: 10
  },

  textInput: {
      width: '90%',
      height: 70,
      borderColor: 'grey',
      borderWidth: 1,
      fontSize: 25
  },

  modalView: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },

  touchableSave: {
      backgroundColor: 'orange',
      paddingHorizontal: 100,
      alignItems: 'center',
      marginTop: 20
  },

   MainContainer :{
 
    alignItems: 'center',
    flex:1,
    paddingTop: 30,
    backgroundColor: '#fff'
 
  },
 
  MainContainer_For_Show_StudentList_Activity :{
    
    flex:1,
    paddingTop: (Platform.OS == 'ios') ? 20 : 0,
    marginLeft: 5,
    marginRight: 5
    
    },
 
  TextInputStyleClass: {
 
  textAlign: 'center',
  width: '90%',
  marginBottom: 7,
  height: 40,
  borderWidth: 1,
  borderColor: '#FF5722',
  borderRadius: 5 ,
 
  },
 
  TouchableOpacityStyle: {
 
    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '90%',
    backgroundColor: '#00BCD4'
 
  },
 
  TextStyle:{
    color:'#fff',
    textAlign:'center',
  },
 
  rowViewContainer: {
    fontSize: 20,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  }
 
 
});



export default styles;

