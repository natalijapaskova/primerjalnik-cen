
import React, { Component } from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {
  FlatList,
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  Platform,
  ListView,
  ActivityIndicator ,
  Alert,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import {
  getIngredientName,
  getAllIngredients,
} from '../../data/MockDataAPI';
import { TextInput } from 'react-native-gesture-handler';




export default  class EditPassword extends Component {
  
    constructor(props) {
      
         super(props)
      
         this.state = {
      
           id: '',
           name: '',
           email: '',
           password: '',
           
      
         }
      
       }
   
       componentDidMount(){
   
        // Received Student Details Sent From Previous Activity and Set Into State.
        this.setState({ 
          id : this.props.navigation.state.params.id,
          name: this.props.navigation.state.params.name,
          email: this.props.navigation.state.params.email,
          password: this.props.navigation.state.params.password,
          
        })
   
       }
    
      static navigationOptions =
      {
         title: 'EditPassword',
      };
   
      UpdateRecord = () =>{
        
              fetch('http://192.168.0.107/PrimerjalnikBaza/update-password.php', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
        
                id : this.state.id,
   
                name : this.state.name,
        
                email : this.state.email,
        
                password : this.state.password
        
               
        
              })
        
              }).then((response) => response.json())
                  .then((responseJson) => {
        
                    // Showing response message coming from server updating records.
                    Alert.alert(responseJson);
        
                  }).catch((error) => {
                    console.error(error);
                  });

                  this.props.navigation.navigate('Categories');
        
        }

        DeleteRecord = () =>{
        
          fetch('http://192.168.0.107/PrimerjalnikBaza/delete-uporabniki.php', {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
          body: JSON.stringify({
        
            id : this.state.id
        
          })
        
          }).then((response) => response.json())
          .then((responseJson) => {
        
         
            Alert.alert(responseJson);
        
          }).catch((error) => {
             console.error(error);
          });
 
          this.props.navigation.navigate('Categories');
 
      }


  
      render() {
  
        return (
     
     <View style={styles.MainContainer}>
     
            <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Urejaj podatke </Text>
      
            <TextInput
              
              placeholder=" Ime:"
              
              value={this.state.name}
     
              onChangeText={ TextInputValue => this.setState({ name : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
           <TextInput
              
              placeholder="Email:"
  
              value={this.state.email}
     
              onChangeText={ TextInputValue => this.setState({ email : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
           <TextInput
              
              placeholder="Geslo:"
  
              value={this.state.password}
     
              onChangeText={ TextInputValue => this.setState({ password : TextInputValue }) }
     
              underlineColorAndroid='transparent'
     
              style={styles.TextInputStyleClass}
            />
     
            
           <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UpdateRecord} >
     
              <Text style={styles.TextStyle}> UREDI  </Text>
     
           </TouchableOpacity>


           <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.DeleteRecord} >
   
            <Text style={styles.TextStyle}> Izbriši uporabnika </Text>
   
         </TouchableOpacity>
    
     
           
      
     
     </View>
                
        );
      }
  
  }
  
