import React from 'react';
import { FlatList, ScrollView, Text, View, TouchableHighlight, Image } from 'react-native';
import styles from './styles';
import { recipes } from '../../data/dataArrays';
import MenuImage from '../../components/MenuImage/MenuImage';
import DrawerActions from 'react-navigation';
import { getCategoryById, getCategoryName } from '../../data/MockDataAPI';

export let trgovine;

export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerLeft: () => <MenuImage
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  });

  constructor(props) {
    super(props);
    
   this.state = {
 
    isLoading: true

  }


  }

  onPressRecipe = item => {
    this.props.navigation.navigate('Recipe', { item });
  };


webCall=()=>{
 

  return fetch('http://192.168.0.107/PrimerjalnikBaza/primerjalnik.php')

         .then((response) => response.json())
         .then((responseJson) => {
           this.setState({
             isLoading: false,
             dataSource: responseJson
           }, function() {
            return trgovine=responseJson;
             // In this block you can do something with new state.
           });
         })
         .catch((error) => {
           console.error(error);
         });
 }
 
 componentDidMount(){
 
  this.webCall();
}

 
  renderRecipes = ({ item }) => (
    
    <TouchableHighlight underlayColor='rgba(73,182,77,0.9)' onPress={() => this.onPressRecipe(item)}>
      <View style={styles.container}>
        <Image style={styles.photo} source={{ uri: item.slika }} />
        <Text style={styles.naziv}>{item.naziv}</Text>

      </View>
    </TouchableHighlight>
  );

  render() {
    return (
      <View>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          numColumns={2}
          data={this.state.dataSource}
          renderItem={this.renderRecipes}
          keyExtractor={item => `${item.id}`}
        />
      </View>
    );
  }
}

 