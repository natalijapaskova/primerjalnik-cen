import React from 'react';
import {
  FlatList,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import {
  getIngredientName,
  getAllIngredients,
} from '../../data/MockDataAPI';
import ViewIngredientsButton from '../../components/ViewIngredientsButton/ViewIngredientsButton';
import {TextInput,Button, Alert} from 'react-native'


export default class IngredientsDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('item'),
      headerTitleStyle: {
        fontSize: 16
      }
    };
  };

  constructor(props) {
    super(props);
  }

  onPressIngredient = item => {
      this.props.navigation.navigate('Ingredient', { item });
  };

  renderIngredient = ({ item }) => (
    <TouchableHighlight underlayColor='rgba(73,182,77,0.9)' onPress={() => this.onPressIngredient(item.id)}>
      <View style={styles.container}>
      <Image style={styles.photo} source={{ uri: item.slika }} />
        <Text style={styles.title}>{item.naziv}</Text>
        <Text style={styles.title}>{item.cena}</Text>
      </View>
    </TouchableHighlight>
  );

  render() {
    const { navigation } = this.props;
    const trgovina = navigation.getParam('id');
    const ingredientsArray = getAllIngredients(trgovina);

    return (
      <View>
        <ViewIngredientsButton
              onPress={() => {
                let id = item.id;
                let title = 'Pregled produktov za ' + item.naziv;
                navigation.navigate('Ingredient', { id, title });
              }}
            />
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          numColumns={3}
          data={ingredientsArray}
          renderItem={this.renderIngredient}
          keyExtractor={item => `${item.id}`}
        />
        <Button
        title="Pogledaj artikli v akciji"
        onPress={() => {
          
          navigation.navigate('Akcija', {trgovina});
          
       }}
      />
      </View>
    );
  }
}
