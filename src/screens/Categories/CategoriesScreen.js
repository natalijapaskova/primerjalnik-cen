import React from 'react';
import {
  FlatList,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import { categories } from '../../data/dataArrays';
import { getNumberOfRecipes } from '../../data/MockDataAPI';
import { Button } from 'react-native';
 import ViewUserButton from '../../components/ViewUserButton/ViewUserButton';


export let uporabniki;

export default class CategoriesScreen extends React.Component {
  static navigationOptions = {
    title: 'Categories'
  };

  constructor(props) {
    super(props);
    this.state = {
 
      isLoading: true
  
    }
  }

  webCall=()=>{
 

    return fetch('http://192.168.0.14/PrimerjalnikBaza/uporabniki.php')
  
           .then((response) => response.json())
           .then((responseJson) => {
             this.setState({
               isLoading: false,
               dataSource: responseJson
             }, function() {
              return uporabniki=responseJson;
               // In this block you can do something with new state.
             });
           })
           .catch((error) => {
             console.error(error);
           });
   }
 
   componentDidMount(){
 
    this.webCall();
  }
   
  onPressCategory = item => {
    const title = item.name;
    const category = item;
     const { navigation } = this.props;
    navigation.navigate('EditPassword', item);
  };

  renderCategory = ({ item }) => (

    <TouchableHighlight underlayColor='rgba(73,182,77,0.9)' onPress={() => this.onPressCategory(item)}>
      <View style={styles.categoriesItemContainer}>
        <Text style={styles.categoriesName}>{item.name}</Text>
        <Text style={styles.categoriesInfo}>Email: {item.email}</Text>
        
        <ViewUserButton
              onPress={() => {
                let id = item.id;
                let title = 'Izbrisi uporabnik';
                navigation.navigate('IngredientsDetails', { id, title });
              }}
            />
      </View>
    </TouchableHighlight>
  );

  render() {
    return (
      <View>
        <FlatList
          data={this.state.dataSource}
          renderItem={this.renderCategory}
          keyExtractor={item => `${item.id}`}
        />
       
      </View>
    );
  }
}
