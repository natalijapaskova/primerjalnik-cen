import React, { Component } from 'react';
 
import { StyleSheet, TextInput, View, Alert, Button, Text, TouchableOpacity} from 'react-native';

import styles from './styles';
import MenuImage from '../../components/MenuImage/MenuImage';



class ProfileActivity extends Component
{
 
  static navigationOptions = ({ navigation }) => ({
    title: 'AdminProfile',
    headerLeft: () => <MenuImage
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  });
 
constructor(props) {
 
    super(props)
 
    this.state = {
 
      UserEmail: '',
      UserPassword: ''
 
    }
 
  }
 
UserLogoutFunction = () =>{
 
 const { UserEmail }  = this.state ;
 const { UserPassword }  = this.state ;
 
 
fetch('http://192.168.0.107/PrimerjalnikBaza/logout.php', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
 
    email: UserEmail,
 
    password: UserPassword
 
  })
 
}).then((response) => response.text())
      .then((responseJson) => {
 
        
      
            
            //this.props.navigation.navigate('LoginActivity', { Email: UserEmail });
 
        
 
      }).catch((error) => {
        console.error(error);
      });
 
 
  }
 
   render()
   {
 
     const {goBack} = this.props.navigation;
 
      return(
         <View style = { styles.container }>

            <Text style = {styles.TextComponentStyle}> { this.props.navigation.state.params.name } </Text>
 
            <Text style = {styles.logo}>Welcome   </Text>
           <Text>{ this.props.navigation.state.params.Email }</Text> 
           <TouchableOpacity   onPress={() => this.props.navigation.navigate('ProdajalciList')} style={styles.loginBtn} >
        <Text style={styles.loginText}>DODAJ PRODAJALCI</Text>
        </TouchableOpacity>

        <TouchableOpacity   onPress={() => this.props.navigation.navigate('Comparison')} style={styles.loginBtn} >
        <Text style={styles.loginText}>KOMPARACIJA</Text>
        </TouchableOpacity>
            
 
         </View>
      );
   }
}


export default ProfileActivity;