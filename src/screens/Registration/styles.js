import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../AppStyles';




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#aee6ae",
    alignItems: "center",
    justifyContent: "center",
  },
 
  logo:{
    fontWeight:"bold",
    fontSize:50,
    color:"green",
    marginBottom:40
  },
  inputView: {
    backgroundColor: "#dcde6a",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },
  
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  }, 
  
  loginBtn:
  {
    width:"80%",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    backgroundColor:"#9ecf4a",
  },
  image :{
    marginBottom: 40,
    height: 100,
    width: 100,
 
  }
 
 
});



export default styles;

