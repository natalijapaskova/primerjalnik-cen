
import React, { Component } from 'react';
 
import { StyleSheet, TextInput, View, Alert, Button, Text, TouchableOpacity, Image} from 'react-native';

import styles from './styles';


class LoginActivity extends Component {
 

  static navigationOptions =
   {
      title: 'LoginActivity',
   };
 
constructor(props) {
 
    super(props)
 
    this.state = {
 
      UserEmail: '',
      UserPassword: ''
 
    }
 
  }
 
UserLoginFunction = () =>{
 
 const { UserEmail }  = this.state ;
 const { UserPassword }  = this.state ;
 
 
fetch('http://192.168.0.107/PrimerjalnikBaza/User_Login.php', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
 
    email: UserEmail,
 
    password: UserPassword
 
  })
 
}).then((response) => response.json())
      .then((responseJson) => {
 
        
       if(responseJson === 'Data Matched')
        {
 
            
          this.props.navigation.navigate('Home', { Email: UserEmail });
        
        }
        else if(responseJson === 'Data Matched For admin'){
          
          this.props.navigation.navigate('ProfileActivity', { Email: UserEmail });
          
        }

        else{
          Alert.alert(responseJson);
        }
 
      }).catch((error) => {
        console.error(error);
      });
 
 
  }
 
  render() {
    return (
 
<View style={styles.container}>
 
        
        
<Image style={styles.image}source = {require('../../../assets/icons/logo.png')}/>
  
  <View style={styles.inputView}>
        <TextInput style={styles.TextInput}
          
          
          placeholder="Elektronski naslov"
 
          onChangeText={UserEmail => this.setState({UserEmail})}
 
          
          underlineColorAndroid='transparent'
 
         // style={styles.TextInputStyleClass}
        />
        </View>
 

        <View style={styles.inputView}>
        <TextInput style={styles.TextInput}
          
          
          placeholder="Geslo"
 
          onChangeText={UserPassword => this.setState({UserPassword})}
 
          
          underlineColorAndroid='transparent'
 
         // style={styles.TextInputStyleClass}
 
          secureTextEntry={true}
        />

</View>


        <TouchableOpacity style={styles.loginBtn} onPress={this.UserLoginFunction}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
  
 
        
 
</View>
            
    );
  }
}






 
export default LoginActivity;