import React, { Component } from 'react';
import styles from './styles';
import { AppRegistry, StyleSheet, TextInput, View, Alert, Button, Text, TouchableHighlight, TouchableOpacity , Image } from 'react-native';
 
export default class RegistrationScreen extends Component {
 
constructor(props) {
 
    super(props)
 
    this.state = {
 
      UserName: '',
      UserEmail: '',
      UserPassword: ''
 
    }
 
  }
 
  UserRegistrationFunction = () =>{
 
 
 const { UserName }  = this.state ;
 const { UserEmail }  = this.state ;
 const { UserPassword }  = this.state ;
 
 
 
fetch('http://192.168.0.107/PrimerjalnikBaza/registracija.php', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
 
    name: UserName,
 
    email: UserEmail,
 
    password: UserPassword
 
  })
 
}).then((response) => response.json())
      .then((responseJson) => {
 
        if(responseJson === 'User Registered Successfully')
        {
 
            
          this.props.navigation.navigate('LoginActivity', { Email: UserEmail });
        }
        else{
 
          Alert.alert(responseJson);
        }
 
        
        
 
      }).catch((error) => {
        console.error(error);
      });
 
 
  }
 
  render() {
    return (
 
<View style={styles.container}>
 
       
        <Image style={styles.image}source = {require('../../../assets/icons/logo.png')}/>
  
        <View style={styles.inputView}>
        <TextInput style={styles.TextInput}
          
          // Adding hint in Text Input using Place holder.
          placeholder="Vpišite uporabniško ime "
 
          onChangeText={UserName => this.setState({UserName})}
 
          // Making the Under line Transparent.
          underlineColorAndroid='transparent'
 
        
        />
        </View>

        <View style={styles.inputView}>
 
        <TextInput style={styles.TextInput}
          
          // Adding hint in Text Input using Place holder.
          placeholder="Vpišite elektronski naslov"
 
          onChangeText={UserEmail => this.setState({UserEmail})}
 
          // Making the Under line Transparent.
          underlineColorAndroid='transparent'
 
          
        />
        </View>

        <View style={styles.inputView}>
 
        <TextInput
          
          // Adding hint in Text Input using Place holder.
          placeholder="Vpišite geslo"
 
          onChangeText={UserPassword => this.setState({UserPassword})}
 
          // Making the Under line Transparent.
          underlineColorAndroid='transparent'
 
        
 
          secureTextEntry={true}
        />
        </View>
 
        <TouchableOpacity title="NAPREJ" onPress={this.UserRegistrationFunction} style={styles.loginBtn} >
        <Text style={styles.loginText}>NAPREJ</Text>
        </TouchableOpacity>
        <Button  style={styles.loginBtn}
          title="Prijava"  
          onPress={() => this.props.navigation.navigate('LoginActivity')} color='#476e0f'
        />  

      
  
 
</View>
            
    );
  }
}