import React from 'react';
import { FlatList, ScrollView, Text, View, TouchableHighlight, Image } from 'react-native';
import styles from './styles';
import { recipes } from '../../data/dataArrays';
import MenuImage from '../../components/MenuImage/MenuImage';
import DrawerActions from 'react-navigation';
import { getCategoryById, getCategoryName } from '../../data/MockDataAPI';

export let artikli;

export default class AkcijaScreen extends React.Component {
   static navigationOptions = ({ navigation }) => {
    return {
     title: navigation.getParam('trgovina'),
    }; 
  };

 constructor(props){
        super(props);
        this.state = {
            artikli: []
           
        };
    }




componentDidMount() {
    fetch('http://192.168.0.18/PrimerjalnikBaza/artiklivakciji.php')
      .then(res => res.json())
      .then(result => {
        this.setState({
         
         
          artikli: result
        });
      });
  }


 
  

  render() {
      var { artikli } = this.state;
       const { navigation } = this.props;
       const id = navigation.getParam('trgovina');
      var name=[];
       artikli.map(item => {
          name.push(item.naziv);
           })
         var  a = artikli.filter(function( obj ) {
  return obj.trgovina_fk === id;
});
    return (
      <View style={{padding:10}}>
       <Text style = {styles.title}>Akcija 20% </Text>
      <FlatList
         vertical
          showsVerticalScrollIndicator={false}
          numColumns={3}
         data={a}
         renderItem={({item}) => 
         <View style={styles.container}>
         <Text style={{height: 50}}>{item.naziv}</Text>
          <Image style={styles.photo} source={{ uri: item.slika }} />
         <View style={{height: 1,backgroundColor:'gray'}}></View>
         </View>
        }
       />
     </View>
    );
  }
}

 