import React,{useState} from 'react';
import {TextInput,Button, Alert} from 'react-native'

import {
  FlatList,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { getIngredientName, getCategoryName, getCategoryById } from '../../data/MockDataAPI';
import BackButton from '../../components/BackButton/BackButton';
import ViewIngredientsButton from '../../components/ViewIngredientsButton/ViewIngredientsButton';


const { width: viewportWidth } = Dimensions.get('window');

export let artikli;

export default class RecipeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTransparent: 'true',
      headerLeft: () => <BackButton
        onPress={() => {
          navigation.goBack();
        }}
      />
    };
  };


  
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0
    };
  }


  webCall=()=>{
 
    return fetch('http://192.168.0.107/PrimerjalnikBaza/artikli.php')

           .then((response) => response.json())
           .then((responseJson) => {
             this.setState({
               isLoading: false,
               dataSource: responseJson
             }, function() {
              return artikli=responseJson;
               // In this block you can do something with new state.
             });
           })
           .catch((error) => {
             console.error(error);
           });
   }

   
 componentDidMount(){
 
  this.webCall();
}

  renderImage = ({ item }) => (
    <TouchableHighlight>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{ uri: item.slika }} />
      </View>
    </TouchableHighlight>
  );

  onPressIngredient = item => {
    var name = getIngredientName(item);
    let ingredient = item;
    const { navigation } = this.props;
    let title = 'Pregled produktov za ' + item.naziv;
    const id = navigation.getParam('item');
    this.props.navigation.navigate('Ingredient',{ item, id });
  };

  render() {
    const { activeSlide } = this.state;
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    const category = getCategoryById(item.id);
    const title = getCategoryName(item.naziv);

    return (
      <ScrollView style={styles.container}>
        <View style={styles.carouselContainer}>
          <View style={styles.carousel}>
            <Carousel
              ref={c => {
                this.slider1Ref = c;
              }}
              data={item.photosArray}
              renderItem={this.renderImage}
              sliderWidth={viewportWidth}
              itemWidth={viewportWidth}
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              firstItem={0}
              loop={false}
              autoplay={false}
              autoplayDelay={500}
              autoplayInterval={3000}
              onSnapToItem={index => this.setState({ activeSlide: index })}
            />
            <Pagination
              dotsLength={item.slika.length}
              activeDotIndex={activeSlide}
              containerStyle={styles.paginationContainer}
              dotColor="rgba(255, 255, 255, 0.92)"
              dotStyle={styles.paginationDot}
              inactiveDotColor="white"
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
              carouselRef={this.slider1Ref}
              tappableDots={!!this.slider1Ref}
            />
                    <Image style={styles.image} source={{ uri: item.slika2 }} />

          </View>
        </View>
        <View style={styles.infoRecipeContainer}>
          <Text style={styles.infoRecipeName}>{item.naziv}</Text>
          <View style={styles.infoContainer}>
            <TouchableHighlight
              onPress={() => navigation.navigate('RecipesList', { category, title })}
            >
              <Text style={styles.category}>{getCategoryName(item.id).toUpperCase()}</Text>
            </TouchableHighlight>
          </View>
 
          <View style={styles.infoContainer}>
            <Text style={styles.infoRecipe}> Lokacija: {item.lokacija} </Text>
          </View>

          <View style={styles.infoContainer}>
            <ViewIngredientsButton
              onPress={() => {
                let id = item.id;
                let title = 'Pregled produktov za ' + item.naziv;
                navigation.navigate('IngredientsDetails', { id, title });
              }}
            />
          </View>
          <View style={styles.infoContainer}>
            <Text style={styles.infoDescriptionRecipe}>{item.lokacija}</Text>
          </View>
        </View>
        <View>
        <Button
        title="Edit"
        onPress={() => {
          <Text>{item.naziv}</Text>
          navigation.navigate('EditTrgovina', item)
          
       }}
      />
             
            
           
         </View>
      </ScrollView>

    );
  }
}

/*cooking steps
<View style={styles.infoContainer}>
  <Image style={styles.infoPhoto} source={require('../../../assets/icons/info.png')} />
  <Text style={styles.infoRecipe}>Cooking Steps</Text>
</View>
<Text style={styles.infoDescriptionRecipe}>{item.description}</Text>
*/
