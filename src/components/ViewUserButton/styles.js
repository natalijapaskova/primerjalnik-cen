import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 2,
    height: 20 ,
    width: 250,
    marginTop: 50,
    marginLeft: 50,
    marginRight: 50,
    borderRadius: 50,
    borderColor: '#2cd18a',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center'
    // backgroundColor: '#2cd18a'
  },
  text: {
    fontSize: 25,
    color: '#2cd18a'
  }
});

export default styles;
