

<!-- PROJECT LOGO -->
<h1>Praktikum-2</h1>
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src=./assets/icons/logo.png alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">Primerjalnik Cen Mobilna Aplikacija</h3>

  <p align="center">
    Mobilna aplikacija za vodenje evidenco cen artiklov v različnih trgovinah.
    <br />
    <a href="https://vimeo.com/563968756"><strong>Demo »</strong></a>
    <br />
    <br />
  
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Vsebina dokumenta</summary>
  <ol>
    <li>
      <a href="#o-projektu">O projektu</a>
      <ul>
        <li><a href="#tehnološki-sklad">Tehnološki sklad</a></li>
      </ul>
    </li>
    <li>
      <a href="#lastnosti">Lastnosti</a>
    </li>
     <li><a href="#uporabe">Uporabe</a>
      <ul>
        <li><a href="#kloniranje">Kloniranje</a></li>
      </ul>
      </li>
    <li><a href="#uporaba-z-expo-client-aplikacijo">Uporaba z Expo Client aplikacijo</a>
    <ul>
    <li><a href="#konfiguriranje-ip-address">Konfiguriranje IP Address</a></li>
    </ul>
    </li>
    <li>  <a href="#pregled-rešitev">Pregled rešitev</a></li>
    <li>  <a href="#avtorji">Avtorji</a></li>
    
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## O projektu



Pred vami je nova mobilna aplikacija Primerjalnik  nadgrajena po  najnovejših tehnološko razvojnih standardih.  

Odslej na enem mestu dobite:
* ceno artikla in trgovinah v  kateri se nahaja se poišče tako, da artikel poišče v podatkovni bazi na podlagi črtne kode s pomočjo fotoaparata mobilnega telefona;
* katera trgovina ima največ artiklov;
* kateri izdelki so v posebnih akcijah;
* kakovost artikla pri ocena artikla od 0 do 5 zvezdic;
* dostop do mapa, kje lahko najdete vse trgovinah v bližini;
*  prikaz kje je posamezni artikel najbolj poceni..

Aplikacija za naprave z operacijskim sistemom [Android](https://expo.io/artifacts/c428753f-b332-4a32-b672-1e266fabcf17 "https://expo.io/artifacts/c428753f-b332-4a32-b672-1e266fabcf17")



### Tehnološki sklad

Glavni okviri, s katerimi smo ustvarili svoj projekt.
* [ React Native](https://reactnative.dev/)
* [Expo](https://docs.expo.io/)
* [JavaScript](https://www.javascript.com/)
* [MySQL](https://www.mysql.com/)
* [PHP](https://www.php.net/)




<!-- GETTING STARTED -->
## Lastnosti

* Izvorni projekt, pripravljen za nadgradnjo na vaši napravi;
* Popolna podpora za splet React Native.
* Plug-n-play predloge po meri.
* Deluje z aplikacijo [Expo Client](https://expo.io/tools).

### Uporabe
 

* Ustvarite nova native React aplikacija.
  ```sh
  npx create-react-native-app
  ```
  *   `npm ios`  -- (`react-native run-ios`)Izdelajte aplikacijo iOS (potreben je računalnik MacOS).
  *  `npm android`  -- (`react-native run-android`) Izdelajte aplikacijo za Android.
  ```sh
  npx create-react-native-app
  ```
  *   `npm web`  -- (`expo start:web`) Zaženite spletno stran v brskalniku.
 

### Kloniranje

1. Kloniranje brez razvojnega okolja z GitBash z 
   ```sh
   git clone https://gitlab.com/nacevadragana/primerjalnikcen.git
   ```
2. Namestite pakete npm
   ```sh
   npm install
   ```




<!-- USAGE EXAMPLES -->
## Uporaba z Expo Client aplikacijo


Expo Client vam omogoča delo z vsemi  [ komponentami in API-ji](https://facebook.github.io/react-native/docs/getting-started)  v  `react-native`, ter  [API-ji JavaScript](https://docs.expo.io/versions/latest), ki so v paketu z aplikacijo Expo. 

Expo Client supports running any project that doesn't have custom native modules added.


-   Prenesite aplikacijo "Expo Client" iz  Play Store ali App Store;
-   Začnite svoj projekt z Expo
    -   Namestite CLI  `npm i -g expo-cli`
    -   Začnite projekt  `expo start`ali  `npm start`
-   Odprite projekt:
    -   Prijavite se v expo in projekt se bo prikazal v aplikaciji;
    -   Ali pa usmerite kamero telefona na QR kodo v terminalu.


<!-- CONTRIBUTING -->
#### Konfiguriranje IP Address

```
exp://192.168.x.x:19000

```

"Manifest" na tem URL-ju pove aplikaciji Expo, kako pridobiti in naložiti sveženj JavaScript aplikacije, tako da tudi če jo naložite v aplikacijo prek URL-ja, kot je `exp://localhost:19000`, bo  aplikacija Expo client  še vedno poskušala pridobite svojo aplikacijo na naslovu IP, ki ga nudi začetni skript.

Mac and Linux:

```
REACT_NATIVE_PACKAGER_HOSTNAME='my-custom-ip-address-or-hostname' npm start

```

Windows:

```
set REACT_NATIVE_PACKAGER_HOSTNAME='my-custom-ip-address-or-hostname'
npm start
```
## Pregled rešitev
<br>
<img src=./assets/ss/register.jpg  height="800">

<img src=./assets/ss/login.jpg height="800">

<img src=./assets/ss/trgovine.jpg height="800">

<img src=./assets/ss/trgovina.jpg height="800">

<img src=./assets/ss/izdelki.jpg height="800">

<img src=./assets/ss/izdelek.jpg height="800">

<img src=./assets/ss/akcija.jpg height="800">

<img src=./assets/ss/cena.jpg height="800">

<img src=./assets/ss/mapa.jpg height="800">

## Avtorji

Nina Stojanova
Natalija Paskova
Dragana Naceva
















