-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2021 at 10:30 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `primerjalnik`
--

-- --------------------------------------------------------

--
-- Table structure for table `artiklov`
--

CREATE TABLE `artiklov` (
  `id` int(11) NOT NULL,
  `naziv` varchar(50) COLLATE utf16_croatian_ci NOT NULL,
  `barcode` varchar(50) COLLATE utf16_croatian_ci NOT NULL,
  `cena` double NOT NULL,
  `trgovina_fk` int(11) NOT NULL,
  `slika` varchar(200) COLLATE utf16_croatian_ci NOT NULL,
  `akcija` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `artiklov`
--

INSERT INTO `artiklov` (`id`, `naziv`, `barcode`, `cena`, `trgovina_fk`, `slika`, `akcija`) VALUES
(1, 'Voda', '24080941', 0.49, 2, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 1),
(2, 'Humus', '4099200072760', 1.79, 2, 'https://veskajjes.si/media/reviews/photos/original/50/cc/e1/1396-spar-veggie-hummus-natur-67-1542715244.jpg', 1),
(3, 'Nutella', '', 3.2, 2, 'https://cdn.shopify.com/s/files/1/0293/2667/1965/products/nuttela.png?v=1602841738', 1),
(4, 'Voda', '24080941', 0.39, 4, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(5, 'Voda', '24080941', 0.59, 3, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(6, 'Voda', '24080941', 0.25, 8, 'https://www.clonelab.at/wp-content/uploads/45.jpghttps://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(7, 'Voda', '24080941', 0.59, 11, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(8, 'Voda', '24080941', 0.4, 6, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(9, 'Voda', '24080941', 0.79, 5, 'https://www.clonelab.at/wp-content/uploads/45.jpg', 0),
(10, 'Humus', '4099200072760', 1.89, 10, 'https://veskajjes.si/media/reviews/photos/original/50/cc/e1/1396-spar-veggie-hummus-natur-67-1542715244.jpg', 0),
(11, 'Humus', '4099200072760', 1.99, 5, 'https://veskajjes.si/media/reviews/photos/original/50/cc/e1/1396-spar-veggie-hummus-natur-67-1542715244.jpg', 0),
(12, 'Humus', '4099200072760', 2.29, 4, 'https://veskajjes.si/media/reviews/photos/original/50/cc/e1/1396-spar-veggie-hummus-natur-67-1542715244.jpg', 0),
(13, 'Humus', '4099200072760', 1.49, 1, 'https://veskajjes.si/media/reviews/photos/original/50/cc/e1/1396-spar-veggie-hummus-natur-67-1542715244.jpg', 0),
(14, 'Popcorn', '2', 0.79, 2, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0),
(15, 'Popcorn', '2', 1.49, 11, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0),
(16, 'Popcorn', '2', 0.89, 4, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0),
(17, 'Popcorn', '2', 0.99, 7, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0),
(18, 'Popcorn', '2', 1.29, 10, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0),
(19, 'Popcorn', '2', 0.99, 3, 'https://5.imimg.com/data5/LA/QX/YO/SELLER-24564999/aone-popcorn-500x500.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ocena`
--

CREATE TABLE `ocena` (
  `ocena` int(11) NOT NULL,
  `artiklov_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ocena`
--

INSERT INTO `ocena` (`ocena`, `artiklov_fk`) VALUES
(4, 1),
(2, 1),
(3, 0),
(4, 2),
(3, 2),
(2, 2),
(5, 2),
(3, 2),
(2, 2),
(1, 2),
(4, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `prodajalci`
--

CREATE TABLE `prodajalci` (
  `id` int(11) NOT NULL,
  `ime` varchar(100) NOT NULL,
  `priimek` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodajalci`
--

INSERT INTO `prodajalci` (`id`, `ime`, `priimek`) VALUES
(1, 'Dragana', 'Naceva'),
(2, 'Natalija', 'Paskova'),
(3, 'Nina', 'Stojanova'),
(0, 'Natalija', 'Natalija'),
(0, 'User', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `trgovine`
--

CREATE TABLE `trgovine` (
  `id` int(11) NOT NULL,
  `naziv` varchar(50) COLLATE utf16_croatian_ci NOT NULL,
  `lokacija` varchar(50) COLLATE utf16_croatian_ci NOT NULL,
  `slika2` varchar(300) COLLATE utf16_croatian_ci DEFAULT NULL,
  `slika` varchar(200) COLLATE utf16_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `trgovine`
--

INSERT INTO `trgovine` (`id`, `naziv`, `lokacija`, `slika2`, `slika`) VALUES
(1, 'Mercator - Koroska', 'Koroska cesta 116, 2000 Maribor', 'http://gpdrava.si/wp-content/uploads/2017/10/Mercator-Jagoda-MB-1024x683.jpg', 'https://www.dnevnik.rs/sites/default/files/styles/single_article_main_image/public/2017-12/374804.jpg?itok=V5ulCMe9'),
(2, 'Hofer - Vodnikov trg', 'Vodnikov trg, 2000 Maribor', 'https://lh3.googleusercontent.com/p/AF1QipPDpSeNCPdfDfdS-sMKopIYq2L8y5-JpYotpaJO=s1600-w400', 'https://upload.wikimedia.org/wikipedia/commons/8/87/Logo_von_Hofer_%28S%C3%BCd%29_seit_2017.png'),
(3, 'Spar - Center', ' Trg svobode 6, 2000 Maribor', 'https://www.shopblogger.de/blog/uploads/1p_2019/SPAR___MARIBOR____IMG_20190103_143300.jpg', 'https://brandslogos.com/wp-content/uploads/images/large/spar-logo-1.png'),
(4, 'Lidl - Koroška cesta', 'Koroška cesta 173, 2000 Maribor', 'http://gpdrava.si/wp-content/uploads/2017/10/Lidl-MB-Koros%CC%8Cka-1024x683.jpg', 'https://seeklogo.com/images/L/lidl-logo-AFAFEAFE19-seeklogo.com.png'),
(5, 'Tuš Planet', 'Na Poljanah 18, 2000 Maribor', 'https://mariborinfo.com/sites/mariborinfo/files/styles/novica/public/slike/naslovne/2019/3/27/53320716_2149040431808172_576371511472947200_n_large.jpg', 'https://seeklogo.com/images/T/tus-logo-07B54F85B2-seeklogo.com.png'),
(6, 'Spar - Veljka Vlahovica', 'Ulica Veljka Vlahovića 62, 2000 Maribor', 'https://www.spar.si/content/dam/sparsiwebsite/mediji/otvoritev-sentjur/sentjur-24-10-20191.jpg/jcr:content/renditions/responsive.665.388.noborder.1f7a800e76e57cef.jpg', 'https://brandslogos.com/wp-content/uploads/images/large/spar-logo-1.png'),
(7, 'Mercator Center', 'Ulica Eve Lovše 1, 2000 Maribor', 'http://www.kalamar.si/images/mcmb/mcmb10.jpg', 'https://www.dnevnik.rs/sites/default/files/styles/single_article_main_image/public/2017-12/374804.jpg?itok=V5ulCMe9'),
(8, 'Lidl - Ptujska cesta ', 'Ptujska cesta 192, 2000 Maribor', 'http://gpdrava.si/wp-content/uploads/2017/10/Lidl-MB_2-1024x683.jpg', 'https://seeklogo.com/images/L/lidl-logo-AFAFEAFE19-seeklogo.com.png'),
(9, 'Hofer - Studenci', ' Preradovičeva ulica 20, 2000 Maribor', 'https://maribor24.si/wp-content/uploads/2020/11/20201105_155614-scaled.jpg', 'https://upload.wikimedia.org/wikipedia/commons/8/87/Logo_von_Hofer_%28S%C3%BCd%29_seit_2017.png'),
(10, 'Interspar', 'Pobreška cesta 18, 2000 Maribor', 'https://www.europark.si/app/uploads/2018/06/interspar-trgovina.jpg', 'https://static.wikia.nocookie.net/dreamlogos/images/6/6a/Interspar-copy-530x270-q80.png/revision/latest?cb=20190705193138'),
(11, 'Tuš - Pobrežje', ' Ulica Veljka Vlahovića 21, 2000 Maribor', 'https://odpiralnicasi.com/photos/084/236/TUS00906-2_%282%29-big.jpg', 'https://seeklogo.com/images/T/tus-logo-07B54F85B2-seeklogo.com.png');

-- --------------------------------------------------------

--
-- Table structure for table `userregistrationtable`
--

CREATE TABLE `userregistrationtable` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userregistrationtable`
--

INSERT INTO `userregistrationtable` (`id`, `name`, `email`, `password`, `usertype`) VALUES
(1, 'Dragana', 'nacevadragana@gmail.com', 'daca', ''),
(2, 'Nina', 'ninas@gmail.com', 'nina1', 'admin'),
(3, 'Natalija', 'natalija@gmail.com', 'Natalija', ''),
(4, 'User', 'User', 'user', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artiklov`
--
ALTER TABLE `artiklov`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trgovina_fk` (`trgovina_fk`);

--
-- Indexes for table `trgovine`
--
ALTER TABLE `trgovine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userregistrationtable`
--
ALTER TABLE `userregistrationtable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artiklov`
--
ALTER TABLE `artiklov`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `trgovine`
--
ALTER TABLE `trgovine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `userregistrationtable`
--
ALTER TABLE `userregistrationtable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `artiklov`
--
ALTER TABLE `artiklov`
  ADD CONSTRAINT `artiklov_ibfk_1` FOREIGN KEY (`trgovina_fk`) REFERENCES `trgovine` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
